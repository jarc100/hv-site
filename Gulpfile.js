var gulp  			= require('gulp'),
    sass 			= require('gulp-sass'),
    php2html 		= require("gulp-php2html"),
	cssmin 			= require('gulp-cssmin'),
    rename 			= require('gulp-rename'),
    autoprefixer 	= require('gulp-autoprefixer'),
	browserSync 	= require('browser-sync').create();

    
// Browser Sync
gulp.task('serve', function() {
    browserSync.init({
        proxy: "http://localhost:8888/sites/utopia-site/dist/"
    });
    gulp.watch("src/scss/**/*.scss", ['sass']);
    gulp.watch("src/php/**/*.php").on('change', browserSync.reload);
});

gulp.task('php2html', function() {  

	gulp.src("src/php/*.php")
		.pipe(php2html())
		.pipe(gulp.dest("dist/"));

});

gulp.task('sass', function () {
	return gulp.src('src/scss/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('src/css/'))
		.pipe(browserSync.stream());
});

gulp.task('autoprefixer', ['sass'], function() {
	return gulp.src('src/css/*.css')
		.pipe(autoprefixer({
			browsers: ['last 2 versions','ie 8','ie 9','android 2.3','android 4','opera 12'],
			cascade: false
		}))
		.pipe(gulp.dest('src/css/build'))
});

gulp.task('cssmin', function() {
	return gulp.src('src/css/build/*.css')
		.pipe(cssmin())
		.pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/assets/css/'));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
	gulp.watch('src/php/**/*.php', ['php2html'])
	gulp.watch('src/scss/**/*.scss', ['sass'])
	gulp.watch('src/css/*.css', ['autoprefixer'])
	gulp.watch('src/css/build/*.css', ['cssmin']);
});

gulp.task('default', ['watch','serve']);

gulp.task('svgresize', function() {
    gulp.src('src/icons/svg/*.svg')
        .pipe(svgscaler({ width: 500 })) // options
        .pipe(gulp.dest('src/icons/'))
});

gulp.task('moveiconcss', [], function() {
    gulp.src("dist/assets/fonts/_icons.css")
        .pipe(rename('_chkndo-icons.scss'))
        .pipe(gulp.dest('src/scss/partials/_base/'));
});

var fontName = 'chkndo-icons';

gulp.task('iconfont', function(){
  gulp.src(['src/icons/svg/*.svg'])
    .pipe(iconfontHtml({
      fontName: fontName,
      path: 'src/templates/template.html',
      targetPath: '../icons/index.html',
    }))
    .pipe(iconfontCss({
      fontName: fontName,
      cssClass: fontName,
      path: 'src/templates/template.css',
      targetPath: '_icons.scss',
      targetPath: '_icons.css',
      fontPath: ''
    }))
    .pipe(iconfont({
      fontName: fontName,
      formats: ['svg', 'ttf', 'eot', 'woff', 'woff2']
     }))
    .pipe(gulp.dest('dist/assets/fonts/'));
});

gulp.task('svg2png', function () {
    gulp.src('src/icons/svg/*.svg')
        .pipe(svg2png())
        .pipe(gulp.dest('src/icons/png'));
});


gulp.task('icons', ['iconfont']);