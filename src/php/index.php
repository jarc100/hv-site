<?php 	
    $bodyType = '';
    $headerType = '';
	include_once "partials/head.php";
?>

    <?php include_once "partials/main-header.php"; ?>
    
    <section class="banner">
        
        <div class="rev_slider_wrapper">
            <div id="rev_slider_1" class="rev_slider" data-version="5.4.5" style="display:none">
                <ul> 
 
                    <!-- BEGIN SLIDE -->
                    <li data-transition="fade">
 
                        <!-- SLIDE'S MAIN BACKGROUND IMAGE -->
                        <img src="assets/img/hero-bg-2.jpg" class="rev-slidebg">
 
                        <!-- BEGIN LAYER -->
                        <div class="tp-caption tp-resizeme" 
 
                        data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Linear"},
                  {"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Linear"}]'
 
    data-visibility="['on', 'on', 'on', 'on']"
 
    data-fontsize="['24', '24', '22', '18']" 
    data-lineheight="['32', '32', '32', '24']" 
    data-color="['#FFF', '#FFF', '#000', '#000']" 
    data-textAlign="['center', 'center', 'center', 'center']"
 
    data-x="['center', 'center', 'center', 'center']" 
    data-y="['center', 'center', 'center', 'center']" 
    data-hoffset="['0', '0', '0', '0']" 
    data-voffset="['0', '0', '0', '0']" 
 
    data-width="['1400', '1366', '992', '480']" 
    data-height="['auto', 'auto', 'auto', 'auto']" 
    data-whitespace="['normal', 'normal', 'nowrap', 'normal']" 
 
    data-paddingtop="[0, 0, 250, 200]"
    data-paddingright="[300, 0, 100, 50]"
    data-paddingbottom="[0, 0, 0, 0]"
    data-paddingleft="[300, 0, 100, 50]" 
 
    data-basealign="slide" 
    data-responsive_offset="off"
 
                        >
                    
                            <img src="assets/img/hero-img.svg" class="img-hero">
                    
                        </div><!-- END LAYER -->
 
                    </li><!-- END SLIDE -->
                </ul>
            </div>
        </div>
        
    </section>

    <section class="home-feature">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-xl-10 col-lg-10 col-md-10">
                    <div class="section-title mb30">
                        
                        <h2 class="margin-bottom-30 text-200 mb50"><span class="primary-green-color text-600">Healthy</span> <span class="primary-blue-color text-200">Vacation</span><span class="weight-300 text-200 r-symbol">®</span></h2>

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <img src="assets/img/nutrition-icon.svg" alt="" class="img-fluid">
                                </div>
                                <div class="col-md-7 col-12 ml-auto">

                                    <p>Nace de la experiencia hotelera y del conocimiento en cuidados de la salud de sus fundadores.</p>
                                    
                                    <p>Es un programa de asesoramiento en materia de salud integral para los hoteles que culmina con una certificación. La certificación, que podrá ser, Green, Gold o Platinum, será el distintivo que indicará que el hotel ha sido capacitado y asesorado por profesionales en salud, y que ha realizado las implementaciones necesarias en sus instalaciones.</p>

                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="service-ex1">
                <div class="row justify-content-md-center">
                    <div class="col-3 col-lg-2 mx-2">
                        
                        <div class="info text-center">
                            
                            <img src="assets/img/badge-one.jpg" class="img-fluid">
                            
                        </div>
                    </div>
                    <div class="col-3 col-lg-2 mx-2">
                        <div class="info text-center">
                            
                            <img src="assets/img/badge-two.jpg" class="img-fluid">
                            
                        </div>
                    </div>
                    <div class="col-3 col-lg-2 mx-2">
                        <div class="info text-center">
                                    
                            <img src="assets/img/badge-three.jpg" class="img-fluid">
                            
                        </div><!-- single info block ends -->
                    </div>
                </div>
            </div>
            
        </div>
    </section>

    <section class="home-feature pt-pb-25">
        <div class="container mb-50">
            <div class="row justify-content-md-center">
                <div class="col-xl-10 col-lg-10 col-md-10">
                    
                    <div class="section-title mb30">
                        
                        <h2 class="margin-bottom-30 text-200 mb30 secondary-blue-color text-600 text-transform-inherit">¿Cuál es la Misión de <span class="primary-green-color text-600">H</span><span class="primary-blue-color text-200">V</span><span class="text-200 r-symbol">®</span>?</h2>
                        
                        <p>Queremos que las personas tengan la posibilidad de mantener o incluir hábitos saludables durante sus vacaciones.</p>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-6 col-md-8 height-350 bg-cover" style="background:url(assets/img/family.jpg);">
                    
                    
                </div>
                <div class="col-6 col-md-4 height-350 bg-cover" style="background:url(assets/img/healthy.jpg);">
                    
                    
                </div>
            </div>
        </div>
    </section>

    <section class="home-feature pt-pb-25">
        <div class="container mb-50">
            <div class="row justify-content-md-center">
                <div class="col-xl-10 col-lg-10 col-md-10">
                    
                    <div class="section-title">
                        
                        <h2 class="margin-bottom-30 text-200 mb30 secondary-blue-color text-600 text-transform-inherit">¿Qué o quién avala a <span class="primary-green-color text-600">H</span><span class="primary-blue-color text-200">V</span><span class="text-200 r-symbol">®</span>?</h2>
                        
                        <p>El programa de asesoramiento <span class="primary-green-color text-600">H</span><span class="primary-blue-color text-200">V</span>® ha sido desarrollado por un grupo de expertos en salud certificados por el IIN (Institute for Integrative Nutrition® de Nueva York) como “Health Coaches”, a los que tendrán la oportunidad de conocer a lo largo del aprendizaje en el e-learning.</p>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-6">
                    
                    <img src="assets/img/certificate.jpg" class="img-fluid">
                    
                </div>
            </div>
        </div>
    </section>

    <section class="home-feature pt-pb-25">
        <div class="container mb-50">
            <div class="row justify-content-md-center">
                <div class="col-xl-10 col-lg-10 col-md-10">
                    
                    <div class="section-title mb30">
                        
                        <h2 class="margin-bottom-30 text-200 mb30 secondary-blue-color text-600 text-transform-inherit">¿Qué es un Health Coach?</h2>
                        
                        <p>El programa de asesoramiento <span class="primary-green-color text-600">H</span><span class="primary-blue-color text-200">V</span>® ha sido desarrollado por un grupo de expertos en salud certificados por el IIN (Institute for Integrative Nutrition® de Nueva York) como “Health Coaches”, a los que tendrán la oportunidad de conocer a lo largo del aprendizaje en el e-learning.</p>
                        
                    </div>
                </div>
            </div>
        </div>


        <div class="container mb-50">
            <div class="row justify-content-md-center">
                <div class="col-xl-10 col-lg-10 col-md-10">
                    
                    <div class="section-title">
                                
                        <h2 class="margin-bottom-30 text-200 mb30 secondary-blue-color text-600 text-transform-inherit">¿Cómo asesora <span class="primary-green-color text-600">H</span><span class="primary-blue-color text-200">V</span><span class="text-200 r-symbol">®</span>?</h2>
                        
                        <p>El asesoramiento es la pieza clave y fundamental de este programa en Salud Integral. HV asesora por medio de un centro de aprendizaje o escuela virtual. Todo esta diseñado de una manera práctica y muy efectiva.</p>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-12 height-350 bg-cover" style="background:url(assets/img/desktop.png);">
                    
                    
                </div>
            </div>
        </div>
        
    </section>

    <section class="home-feature pt-pb-25">

        <div class="container mb-50">
            <div class="row justify-content-md-center">
                <div class="col-xl-10 col-lg-10 col-md-10">
                    
                    <div class="section-title">
                                
                        <h2 class="margin-bottom-30 text-200 mb30 secondary-blue-color text-600 text-transform-inherit"><span class="primary-green-color text-600">H</span><span class="primary-blue-color text-200">V</span><span class="text-200 r-symbol">®</span> Social Media</h2>
                        
                        <p>Healthy Vacation cuenta con el programa de “HV Social Media” donde Health Coaches, Healthy Trip Advisors, y Fitness Advisors expondrán a los nuevos hoteles certificados.</p>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-12 height-350 bg-cover" style="background:url(assets/img/iphone.png);">
                    
                    
                </div>
            </div>
        </div>
        
    </section>

    <section class="home-feature pt-pb-25">

        <div class="container mb-50">
            <div class="row justify-content-md-center">
                <div class="col-xl-7 col-lg-8 col-md-10">

                    <div class="section-title text-center mb30">
                                
                        <h2 class="mb15 text-200 mb30 secondary-blue-color text-600 text-transform-inherit">Contacto</h2>
                        
                        <p>Para más información, inspección, o aplicación de HV en tu hotel, contáctanos</p>
                        
                    </div>
                    
                    <form class="">
                        <div class="row ">
                            <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="exampleInputName" placeholder="Nombre">
                            </div>
                            <div class="form-group col-md-6">
                            <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email">
                            </div>
                            <div class="form-group col-md-12">
                            <textarea class="form-control" id="exampleTextarea" rows="6" placeholder="Mensaje"></textarea>
                            </div>
                        </div>
                        <div class="d-flex">
                            <button type="submit" class="btn btn-lg btn-primary ml-auto bg-green">Enviar</button>
                        </div>
                    </form><!-- Contact Form ends-->
                </div>
            </div>

            <div class="container mt-50 mb-50">
                <div class="row justify-content-md-center">
                    <div class="col-4">
                        <img src="assets/img/logo.png" class="img-fluid">                      
                    </div>
                </div>
            </div>

        </div>
        
    </section>

    <?php include_once "partials/main-footer.php"; ?>
    
<?php 
    include_once "partials/footer.php"; 
?>