<footer class="footer footer-classic secondary-blue-bg">
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-lg-7 col-md-8">
        
            <h1 class="footer-mark"></h1>

        </div>
        <!-- BACK TO TOP BUTTON -->
        <a href="#pageTop" class="backToTop green-bg"><i class="fa fa-chevron-up"></i></a>
    </div>	
    <!-- COPY RIGHT -->
    <div class="copyright">
        <hr>
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 ">
                <div class="copyRight_text text-center">
                    <p> © <a class="text-primary" href="" target="_blank"></a>.</p>
                </div>
            </div>
        </div>

    </div>
</div>

</footer>
</div>