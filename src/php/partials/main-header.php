<header id="pageTop" class="header">	
		
    <nav class="navbar navbar-expand-md main-nav navbar-sticky header-transparent">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="burger-menu icon-toggle"><i class="icon-menu icons"></i></i></span>
            </button>
            <a class="navbar-brand" href="index.html">
                <img src="assets/img/logo-white.svg" class="hide-on-navbar-sticky" height="85" >
                <img src="assets/img/logo-color.svg" class="show-on-navbar-sticky" height="85" >
            </a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.html">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    
                    <?php /*
                    <li class="nav-item dropdown drop_single ">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">portfolio</a>
                        <ul class="dropdown-menu dd_first">
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects style 1</a>
                                <ul class="dropdown-menu submenu">
                                    <li><a href="portfolio-fullwidth.html">Fullwidth</a></li>
                                    <li><a href="portfolio-boxed-2-column.html">Boxed 2 Column</a></li>
                                    <li><a href="portfolio-boxed-3-column.html">Boxed 3 Column</a></li>
                                    <li><a href="portfolio-boxed-4-column.html">Boxed 4 Column</a></li>
                                    <li><a href="portfolio-boxed-sidebar.html">Boxed With Sidebar</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects Style 2</a>
                                <ul class="dropdown-menu submenu">
                                    <li><a href="portfolio-card-fullwidth.html">Fullwidth</a></li>
                                    <li><a href="portfolio-card-boxed-2-column.html">Boxed 2 Column</a></li>
                                    <li><a href="portfolio-card-boxed-3-column.html">Boxed 3 Column</a></li>
                                    <li><a href="portfolio-card-boxed-4-column.html">Boxed 4 Column</a></li>
                                    <li><a href="portfolio-card-boxed-sidebar.html">Boxed With Sidebar</a></li>
                                </ul>
                            </li>
                            <li><a href="portfolio-project-details.html" >Project Details</a></li>
                        </ul>							
                    </li>

                    <li class="nav-item dropdown drop_single ">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">Services</a>
                        <ul class="dropdown-menu dd_first">
                            <li><a href="service.html">Services</a></li>
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Service Details</a>
                                <ul class="dropdown-menu submenu">
                                    <li><a href="service-details-left-sidebar.html">Service Left Sidebar</a></li>
                                    <li><a href="service-details-right-sidebar.html">Service Right Sidebar</a></li>
                                    <li><a href="service-details-fullwidth.html">Service Fullwidth</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item dropdown drop_single ">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">Pages</a>
                        <ul class="dropdown-menu dd_first">
                            <li><a href="page-aboutus.html">About Us</a></li>
                            <li><a href="page-work-process.html">Work process</a></li>
                            <li><a href="page-team.html">Team Members</a></li>
                            <li><a href="page-member-details.html">Member Profile</a></li>
                            <li><a href="page-faq.html">FAQ</a></li>
                            <li><a href="page-contactus.html">Contact</a></li>
                            <li><a href="page-comingsoon.html">Comingsoon page</a></li>
                            <li><a href="page-error.html">404 page</a></li>
                        </ul>
                    </li>

                    <li class="nav-item dropdown drop_single ">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">Blogs</a>
                        <ul class="dropdown-menu dd_first">
                            <li><a href="blog-fullwidth.html">Blog Fullwidth</a></li>
                            <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                            <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                            <li><a href="blog-post-left-sidebar.html">Blog Post Left Sidebar</a></li>
                            <li><a href="blog-post-right-sidebar.html">Blog Post Right Sidebar</a></li>
                        </ul>
                    </li>
                    
                    <li class=" dropdown megaDropMenu nav-item " >
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="javascript:void(0)">Elements</a>
                        <ul class="row dropdown-menu justify-content-md-between">
                            <li class="">
                                <h6>General Elements</h6>
                                <ul class="list-unstyled">
                                    <li><a href="basic-accordions.html">Accordions</a></li>
                                    <li><a href="basic-alert.html">Alerts</a></li>
                                    <li><a href="basic-animation.html">Animation</a></li>
                                    <li><a href="basic-badge.html">Badge</a></li>
                                    <li><a href="basic-background.html">Background</a></li>
                                    <li><a href="basic-buttons.html">Buttons</a></li>
                                    <li><a href="basic-buttons-group.html">Button Group</a></li>
                                    <li><a href="basic-card.html">Card</a></li>
                                    <li><a href="basic-carousel.html">Carousel</a></li>
                                </ul>
                            </li>
                            <li class="">
                                <h6>General Elements</h6>
                                <ul class="list-unstyled">
                                    <li><a href="basic-countdown.html">Countdown</a></li>
                                    <li><a href="basic-counter.html">Counter</a></li>
                                    <li><a href="basic-divider.html">Divider & Seperator</a></li>
                                    <li><a href="basic-forms.html">Forms</a></li>
                                    <li><a href="basic-heading.html">Heading</a></li>
                                    <li><a href="basic-iconset.html">Iconset - FontAwesome </a></li>
                                    <li><a href="basic-iconset-simple-lines.html">Iconset - Simple Line </a></li>
                                    <li><a href="basic-icon-style.html">Icon Sizes & styles </a></li>
                                </ul>
                            </li>
                            <li class="">
                                <h6>General Elements</h6>
                                <ul class="list-unstyled">
                                    <li><a href="basic-image.html">Image</a></li>
                                    <li><a href="basic-map.html">Map</a></li>
                                    <li><a href="basic-modal.html">Modal</a></li>
                                    <li><a href="basic-progressbar.html">Progress</a></li>
                                    <li><a href="basic-tabs.html">Tabs & wizards</a></li>
                                    <li><a href="basic-tables.html">Tables</a></li>
                                    <li><a href="basic-typography.html">Typography</a></li>
                                    <li><a href="basic-video.html">Video & Audio</a></li>
                                </ul>
                            </li>
                            <li class="">
                                <h6>Theme Kit Elements</h6>
                                <ul class="list-unstyled">
                                    <li><a href="kit-accordions.html">Accordions</a></li>
                                    <li><a href="kit-breadcrumb.html">Breadcrumb</a></li>
                                    <li><a href="kit-blog.html">Blogs</a></li>
                                    <li><a href="kit-counter.html">Counter Up</a></li>
                                    <li><a href="kit-contact.html">Contact</a></li>
                                    <li><a href="kit-gallery.html">Gallery</a></li>
                                    <li><a href="kit-navbar.html">Navbar</a></li>
                                    <li><a href="kit-pagination.html">Pagination</a></li>
                                </ul>
                            </li>
                            <li class="">
                                <h6>Theme Kit Elements</h6>
                                <ul class="list-unstyled">
                                    <li><a href="kit-pricing.html">Pricing Table</a></li>
                                    <li><a href="kit-progressbar.html">Theme Progressbar</a></li>
                                    <li><a href="kit-section-title.html">Section Heading</a></li>
                                    <li><a href="kit-service.html">Service contents</a></li>
                                    <li><a href="kit-team.html">Team</a></li>
                                    <li><a href="kit-testimonials.html">Testimonials</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- header search -->
                    <li class="nav-item search_hook">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#searchModal" class="btn-search nav-link"><i class="fa fa-search"></i></a>
                        <form  class="search_form">
                            <input type="text" name="search" placeholder="Search...">
                            <button class="no-bg btn-search" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </li> */ ?>

                </ul>
            </div>
            <!-- header search ends-->
        </div>
    </nav>
        
</header> 
    
    <div class="main-wrapper ">