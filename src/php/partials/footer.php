<!-- JAVASCRIPTS -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsQdSlW4vj5RvXp2_pLnv1s1ErfxjM5_o"></script>
<script src="assets/plugins/jquery/jquery.min.js"></script>
<script src="assets/plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
<script src="assets/plugins/bootstrap/js/tether.min.js"></script>
<script src="assets/plugins/bootstrap/js/popper.min.js" ></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
<script src="assets/plugins/slick/slick.min.js"></script>
<script src="assets/plugins/circle-progress/jquery.appear.js"></script>
<script src="assets/plugins/isotope/isotope.min.js"></script>
<script src="assets/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="assets/plugins/counterUp/waypoint.js"></script>
<script src="assets/plugins/counterUp/jquery.counterup.js"></script>
<script src="assets/plugins/smoothscroll/SmoothScroll.js"></script>

<script src="assets/plugins/syotimer/jquery.syotimer.min.js"></script>

<script type="text/javascript" src="assets/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>


<script src="assets/js/custom.js"></script>

    </body>
</html>
