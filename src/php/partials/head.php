<!DOCTYPE html>
<html lang="en">
<head>

	<!-- SITE TITTLE -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>

	<!-- PLUGINS CSS STYLE -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/plugins/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"> -->
	<link href="assets/plugins/selectbox/select_option1.css" rel="stylesheet">
	<link href="assets/plugins/slick/slick.css" rel="stylesheet" media="screen">
	<link href="assets/plugins/slick/slick-theme.css" rel="stylesheet" media="screen">
	<link href="assets/plugins/prismjs/prism.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/plugins/fancybox/jquery.fancybox.min.css" />
	<link href="assets/plugins/selectbox/select_option1.css" rel="stylesheet">
	<link href="assets/plugins/isotope/isotope.min.css" rel="stylesheet">
	<link href="assets/plugins/animate.css" rel="stylesheet">

	<!-- REVOLUTION SLIDER -->
	<link rel="stylesheet" href="assets/plugins/revolution/css/settings.css">
	<link rel="stylesheet" href="assets/plugins/revolution/css/layers.css">
	<link rel="stylesheet" href="assets/plugins/revolution/css/navigation.css">
	
	<!-- CUSTOM CSS -->
	<link rel="stylesheet" href="assets/css/default.css" id="option_color">
	<link href="assets/css/style.min.css" rel="stylesheet">
    
    <!-- FAVICON -->
	<link href="img/favicon.png" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
    
    <body id="body" class="home-classic  <?php
        global $bodyType;
        if ( $bodyType == '' ) {
            echo '';
        } else if ( $bodyType == '404' ) {
            echo $bodyType;
        } ?>">
 
        <div id="preloader" class="smooth-loader-wrapper">
            <div class="smooth-loader">
                <div class="loader1">
                <div class="loader-target">
                    <div class="loader-target-main"></div>
                    <div class="loader-target-inner"></div>
                    </div>
                </div>
            </div>
        </div>
        
